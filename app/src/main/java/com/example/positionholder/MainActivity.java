package com.example.positionholder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button log, register;
    DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // db = new DatabaseHelper(this);

    }
    public void clickRegister(View v){
        Intent reg = new Intent(this, RegisterActivity.class);
        startActivity(reg);
    }
    public void clickLog(View v){
        Intent log = new Intent(this, UserSpace.class);
        //vari check se log corretto
        startActivity(log);
    }
}
