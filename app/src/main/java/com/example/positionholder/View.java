package com.example.positionholder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import java.security.KeyStore;

public class View extends AppCompatActivity {
    DatabaseHelper myDB;
    Button bUpdate;
    EditText eUsername, name, email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        myDB = new DatabaseHelper(this);

        password = (EditText) findViewById(R.id.etPasswordR);
        name = (EditText) findViewById(R.id.etNameR);
        email = (EditText) findViewById(R.id.etEmail);
        eUsername = (EditText) findViewById(R.id.etUserNameR);

        bUpdate = (Button) findViewById(R.id.bUpdate);
        eUsername = (EditText) findViewById(R.id.etUserNameR);

    }
}
