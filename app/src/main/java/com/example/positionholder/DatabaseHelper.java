package com.example.positionholder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "register.db";
    public static final int DB_VERSION = 1;
    public static final String TABLE_NAME = "register_table";
    public static final String COL_1 = "NAME";
    public static final String COL_2 = "USER_NAME";
    public static final String COL_3 = "PASSWORD";
    public static final String COL_4 = "EMAIL";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+ TABLE_NAME + " (NAME TEXT, USER_NAME TEXT PRIMARY KEY AUTOINCREMENT, PASSWORD TEXT, EMAIL TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        this.onCreate(db);
    }

  /*  public boolean Register( String name, String userName, String password, String email){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, name);
        contentValues.put(COL_2, userName);
        contentValues.put(COL_3, password);
        contentValues.put(COL_4, email);

        long result = db.insert(TABLE_NAME, null, contentValues);

        if (result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME, null);
        return res;
    }

    public  boolean updateData (String name, String userName, String password, String email){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, name);
        contentValues.put(COL_2, userName);
        contentValues.put(COL_3, password);
        contentValues.put(COL_4, email);
        db.update(TABLE_NAME, contentValues, "userName = ?",new String[]{userName});
        return true;
    }*/
}
